/**
 * bpmn-js-seed - async
 *
 * This is an example script that loads a bpmn diagram <diagram.bpmn> and opens
 * it using the bpmn-js viewer.
 *
 * YOU NEED TO SERVE THIS FOLDER VIA A WEB SERVER (i.e. Apache) FOR THE EXAMPLE TO WORK.
 * The reason for this is that most modern web browsers do not allow AJAX requests ($.get and the like)
 * of file system resources.
 */
(function(BpmnViewer, $) {

  // create viewer
  var bpmnViewer = new BpmnViewer({
    container: '#canvas'
  });


  // import function
  function importXML(xml) {

    // import diagram
    bpmnViewer.importXML(xml, function(err) {

      if (err) {
        return console.error('could not import BPMN 2.0 diagram', err);
      }

      var eventBus = bpmnViewer.get('eventBus');

    // you may hook into any of the following events
    var events = [
      'element.click',
    ];

    events.forEach(function(event) {

      eventBus.on(event, function(e) {
        // e.element = the model element
        // e.gfx = the graphical element
		var elementRegistry = bpmnViewer.get('elementRegistry');
		var flowElement = elementRegistry.get(e.element.id);

		if(flowElement.businessObject.$instanceOf('bpmn:DataObjectReference')) {
			if (flowElement.businessObject.dataState!=undefined) {
				$.get('http://localhost:8080/dataObjectClicked?id='+flowElement.businessObject.dataObjectRef.id+'&state='+flowElement.businessObject.dataState.name);
				alert(flowElement.businessObject.dataObjectRef.id+' updated with state '+flowElement.businessObject.dataState.name);
			}
		} else if (flowElement.businessObject.$instanceOf('bpmn:Activity')){
			var r = confirm("Start task? (Cancel= stop task)");
			if (r == true) {
				$.get('http://localhost:8080/activityClicked?id='+flowElement.businessObject.id+'_s');
				alert(flowElement.businessObject.id+'_s sent');
			} else {
				$.get('http://localhost:8080/activityClicked?id='+flowElement.businessObject.id+'_t');
				alert(flowElement.businessObject.id+'_t sent');
			}
		} else if (flowElement.businessObject.$instanceOf('bpmn:Event')){
            var data = prompt("Event data?", "");
			$.get('http://localhost:8080/eventClicked?id='+flowElement.businessObject.id+'&data='+data);
			alert(flowElement.businessObject.id+' sent');
		} else {
			$.get('logData?param='+flowElement.businessObject.$instanceOf('bpmn:Task'));
        }
      });
    });


	  var canvas = bpmnViewer.get('canvas'),
          overlays = bpmnViewer.get('overlays');


      // zoom to fit full viewport
      canvas.zoom('fit-viewport');

      // attach an overlay to a node
      overlays.add('SCAN_OK', 'note', {
        position: {
          bottom: 0,
          right: 0
        },
        html: '<div class="diagram-note">Mixed up the labels?</div>'
      });

      // add marker
      canvas.addMarker('SCAN_OK', 'needs-discussion');
    });
  }


  // load external diagram file via AJAX and import it
  $.get('bpmnDiagram', importXML, 'text');


})(window.BpmnJS, window.jQuery);
