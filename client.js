var Client = require('node-rest-client').Client;
var client = new Client();
var express = require('express');
var app = express();
var mqtt = require('mqtt');
var fs = require('fs');
var xml2js = require('xml2js');
var eventTopic = '';
app.use(express.static(__dirname));
var artifactTopics = new Array();
var dataTopic = '';

var USER = 'admin';
var PASS = 'password';
var clientId = 'ntstmqtt_' + Math.random().toString(16).substr(2, 8);
var BrokerHost = 'localhost';
var BrokerPort = 1883;

if(process.argv.length > 4) {
	BrokerHost = process.argv[2];
	USER = process.argv[3];
	PASS = process.argv[4];
}

var opts = { host: BrokerHost, port: BrokerPort, username: USER, password : PASS, keepalive: 30, clientId: clientId };
var mqttclient = mqtt.connect(opts);

var mappingPath = 'mapping.xml';
var example = '';
var objEvent = {};
var objPayload = {};
var sensorData = {};

var server = app.listen(8080, function () {
  var port = server.address().port;
  console.log("Test client listening on port "+port);
})

client.registerMethod("sendDataToBroker", "http://localhost:8081/api/data", "GET");

function initConnections(mapping) {
    artifactTopics = new Array();
	console.log('startmap');
	var localArtifact =  mapping['martifact:definitions']['martifact:localArtifact'][0];
	dataTopic=localArtifact['$'].name+'/'+localArtifact['$'].id+'/data';
    var artifact = new Array();
    artifact.push(localArtifact['$'].name+'/'+localArtifact['$'].id+'/status');
	artifactTopics[localArtifact['$'].name]=artifact;
	var remoteArtifacts = mapping['martifact:definitions']['martifact:remoteArtifact'];
    for(var key in remoteArtifacts) {
        artifact = new Array();
        for (var key2 in remoteArtifacts[key]['martifact:remoteId']) {
            artifact.push(remoteArtifacts[key]['$'].name+'/'+remoteArtifacts[key]['martifact:remoteId'][key2]['$'].id+'/status')
        }
      artifactTopics[remoteArtifacts[key]['$'].name]=artifact;
	}
	var stakeHolders = mapping['martifact:definitions']['martifact:stakeholder'];
    for(var key in stakeHolders) {
      eventTopic=stakeHolders[key]['$'].name+'/'+stakeHolders[key]['$'].processInstance
	}
}

function getSensorData(mapping){
    sensorData=mapping['martifact:definitions']['martifact:localArtifact'][0]['martifact:sensorDatum']
    /*
    for(var key in sensorData) {
        sensorData[sensorData[key]['$'].name].type=sensorData[key]['$'].type;

        sensorData[sensorData[key]['$'].name].values
	}
    */
}

/*
	app.get('*', function(req, res) {
		res.sendfile('./index.html');
	});

 app.get('diagram.bpmn', function(req, res) {
		res.sendfile('./diagram.bpmn');
	});
*/

app.get('/activityClicked', function (req, res) {
	var payloadEvent = {};
	payloadEvent.id = 'foobar';
	payloadEvent.eventid = req.param('id');
	payloadEvent.timestamp = Date.now();
	objPayload.payloadData = payloadEvent;
	objEvent.event = objPayload;
	mqttclient.publish(eventTopic, JSON.stringify(objEvent));
	console.log(eventTopic, JSON.stringify(objEvent));
	res.end('ok');
})

app.get('/eventClicked', function (req, res) {
	var payloadEvent = {};
	payloadEvent.id = 'foobar';
	payloadEvent.eventid = req.param('id');
    payloadEvent.data = req.param('data');
	payloadEvent.timestamp = Date.now();
	objPayload.payloadData = payloadEvent;
	objEvent.event = objPayload;
	mqttclient.publish(eventTopic, JSON.stringify(objEvent));
	console.log(eventTopic, JSON.stringify(objEvent));
	res.end('ok');
})

app.get('/dataObjectClicked', function (req, res) {
	var payloadEvent = {};
	payloadEvent.id = 'foobar';
	payloadEvent.status = req.param('state');
	payloadEvent.timestamp = Date.now();
	objPayload.payloadData = payloadEvent;
	objEvent.event = objPayload;
    for (var key in artifactTopics[req.param('id')]) {
       mqttclient.publish(artifactTopics[req.param('id')][key], JSON.stringify(objEvent));
	   console.log(artifactTopics[req.param('id')][key], JSON.stringify(objEvent));
	}
	res.end('ok');
})

app.get('/sensorDataSent', function (req, res) {
   var obj = {}
   for(var key in sensorData) {
       obj[sensorData[key]['$'].name] = req.param(sensorData[key]['$'].name);
   }
    var args = {};
    args['parameters'] = obj;
    client.methods.sendDataToBroker(args, function (data, response) {
        // parsed response body as js object
        console.log(data);
        // raw response
        console.log(response);
    });
    //console.log('data sent');
	res.redirect('back');
})

app.get('/logdata', function (req, res) {
	console.log(req.param('param'));
	res.end('ok');
})

app.get('/setExample', function (req, res) {
    example = req.param('id')
	console.log(example);
    var xmlMapping=fs.readFileSync(example+'.xml', 'utf8');
    var parseString = xml2js.parseString;
    parseString(xmlMapping, function (err, result) {
      //TODO, aggiungere la gestione dell'errore
      console.log('parsed');
	  initConnections(result);
      getSensorData(result);
    });
	res.redirect('interface.html');
})

app.get('/bpmnDiagram', function (req, res) {
	console.log(req.param('example'));
    res.redirect(example+'.bpmn');
})

app.get('/formFields', function (req, res) {
    console.log(JSON.stringify(sensorData));
	res.end(JSON.stringify(sensorData));
})


mqttclient.on('connect', function () {
  console.log('MQTT client connected');
});

mqttclient.on('reconnect', function () {
  console.log('MQTT client reconnected');
});


mqttclient.on('error', function (error) {
  console.log('MQTT client error' + error);
});

mqttclient.on('message', function (topic, message) {
  console.log([topic, message].join(": "));
});
