(function($) {
    function generateFields(data){
        var form = '<form action="sensorDataSent" method="get">';
        var fieldAttrs = JSON.parse(data);
        //$('#fields').append('<form action="sensorDataSent" method="get">');
        for(var key in fieldAttrs) {
            form = form + fieldAttrs[key]['$'].name+':';
            //$('#fields').append(fieldAttrs[key]['$'].name+':');

            if(fieldAttrs[key]['$'].type==="boolean"){
                //$('#fields').append('<input type="checkbox" name="'+fieldAttrs[key]['$'].name+'" value="true"/>');
                form = form+'<input type="checkbox" name="'+fieldAttrs[key]['$'].name+'" value="true"/>';
            } else {
                if (typeof fieldAttrs[key]['martifact:sensorValue'] != 'undefined') {
                    form = form+'<select name="'+fieldAttrs[key]['$'].name+'">';
                    //$('#fields').append('<select name="'+fieldAttrs[key]['$'].name+'">');
                    for(var key2 in fieldAttrs[key]['martifact:sensorValue']) {
                        //$('#fields').append('<option value="'+fieldAttrs[key]['martifact:sensorValue'][key2]['$'].value+'">'+fieldAttrs[key]['martifact:sensorValue'][key2]['$'].name+'</option>');
                        form = form+'<option value="'+fieldAttrs[key]['martifact:sensorValue'][key2]['$'].value+'">'+fieldAttrs[key]['martifact:sensorValue'][key2]['$'].name+'</option>';
                    };
                    form = form+'</select>';
                    //$('#fields').append('</select>');
                } else {
                    form = form+'<input type="text" name="'+fieldAttrs[key]['$'].name
                    if(typeof fieldAttrs[key]['$'].default != 'undefined') {
                        form = form+'" value="'+fieldAttrs[key]['$'].default
                    };
                    form = form+'"/>';
                    //$('#fields').append('<input type="text" name="'+fieldAttrs[key]['$'].name+'"/>');
                }

            }
        };

        $('#fields').append(form+'<input type="submit" value="Submit"/></form>');
    }
    var foo = $.get("formFields", generateFields, 'text');

})(window.jQuery);
